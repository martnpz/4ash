FROM crystallang/crystal:nightly-alpine

WORKDIR /fourash
COPY . .

RUN apk update && apk add sqlite-dev

RUN shards install

RUN crystal build --release src/fourash.cr

CMD ["./fourash"] 

EXPOSE 1488
