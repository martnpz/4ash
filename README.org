#+title: fourash
Paja's imageboard.
#+html: <a href="https://kemalcr.com/">
#+html:   <img src="./public/kemal-tag.svg" alt="Kemal web" width="120px" />
#+html: </a>
#+html:   <img src="./public/version-tag.svg" alt="Version" width="120px"/>
#+html:   <img src="./public/license-tag.svg" alt="License" width="120px"/>
#+html: <a href="http://fourash.martnpz.com/">
#+html:   <img src="./public/fourash-tag.svg" alt="Fouarash web" width="120px"/>
#+html: </a>

* Installation:
  #+BEGIN_SRC sh
   $ shards install
   $ crystal run src/fourash.cr
  #+END_SRC

* Contributing

1. [[https://gitlab.com/ashandme/4ash/-/forks/new][Fork it!]]
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

* Contributors

- [[https://gitlab.com/ashandme][ashandme]] - creator and maintainer
- [[https://gitlab.com/martnpz][Martín]] - Jeffrey Epstein
