require "kemal"
require "sqlite3"
require "uuid"

# Check if datdabase exist.
if !File.exists?("data.db")
    puts "Creating database..."
    DB.open "sqlite3://./data.db" do |db|
        db.exec "
    create table post (
	       id	    INTEGER NOT NULL,
	       poster	TEXT NOT NULL,
           title	TEXT NOT NULL,
           body	TEXT NOT NULL,
           image	TEXT NOT NULL,
           date	TEXT NOT NULL,
           PRIMARY KEY(id AUTOINCREMENT)
    )"

        db.exec "
    create table comment (
	       id	    INTEGER NOT NULL,
	       post_id	INTEGER NOT NULL,
           poster	TEXT NOT NULL,
           text	TEXT NOT NULL,
           image	TEXT NOT NULL,
           date	TEXT NOT NULL,
           PRIMARY KEY(id AUTOINCREMENT)
    )"
    end
end

# Validate image.
def valid_image(image)
    if image.tempfile.size >= 1_000_000
        image.tempfile.delete
        return false
    elsif /[.](jpg|jpeg|png|svg)$/ !~ image.filename
        image.tempfile.delete
        return false
    end
    return true
end

# Generate random small id.
def rand_id
    id     = [] of String
    letter = ('a'..'z').to_a
    id << letter.sample.to_s
    id << rand(9).to_s
    id << letter.sample.to_s
    id << rand(9).to_s

    id.join("")
end

class Posts
    DB.mapping({
                   id:     Int64,
                   poster: String,
                   title:  String,
                   body:   String,
                   image:  String,
                   date:   String,
    })
end

class Comments
    DB.mapping({
                   id:      Int64,
                   post_id: Int64,
                   poster:  String,
                   text:    String,
                   image:   String,
                   date:    String,
    })
end

# Landing page.
get "/" do
    content = [] of Posts
    DB.open "sqlite3://./data.db" do |db|
        content = Posts.from_rs(db.query("select * from post order by id desc"))
        end
    render "src/views/index.ecr"
end

get "/post" do
    poster_id = rand_id
    render "src/views/create.ecr"
end

post "/post" do |env|
    env.redirect "/post"
end

# Upload post.
post "/create" do |env|
    title = env.params.body["title"].as(String)
    body = env.params.body["body"].as(String)
    poster = env.params.body["poster"].as(String)
    image = env.params.files["image"]

    if valid_image(image)
        image_name = "#{UUID.random}.png"
        image_path = ::File.join [Kemal.config.public_folder, "images/post/", File.basename(image_name)]

        File.open(image_path, "w") do |f|
            IO.copy(image.tempfile, f)
        end
    else
        image_name = "default.png"
    end

    DB.open "sqlite3://./data.db" do |db|
        new_post = [] of DB::Any
        new_post << poster
        new_post << title
        new_post << body
        new_post << image_name
        new_post << Time.local

        db.exec "insert into post(poster, title, body, image, date)
                    values (?, ?, ?, ?, ?)", args: new_post
    end

    env.redirect "/"
end

# Upload comment by post.
post "/comment" do |env|
    post_id = env.params.body["post_id"].as(String)
    text = env.params.body["text"].as(String)
    poster = env.params.body["poster"].as(String)
    image = env.params.files["image"]

    if valid_image(image)
        image_name = "#{UUID.random}.png"
        image_path = ::File.join [Kemal.config.public_folder, "images/comment/", File.basename(image_name)]

        File.open(image_path, "w") do |f|
            IO.copy(image.tempfile, f)
        end
    else
        image_name = ""
    end

    DB.open "sqlite3://./data.db" do |db|
        new_comment = [] of DB::Any
        new_comment << post_id
        new_comment << poster
        new_comment << text
        new_comment << image_name
        new_comment << Time.local

        db.exec "insert into comment(post_id, poster, text, image, date)
                    values (?, ?, ?, ?, ?)", args: new_comment
    end

    env.redirect "/paja/#{post_id}?"
end

# Individual page for posts.
# Each post is named after paja.
get "/paja/:id" do |env|
    post_id = env.params.url["id"]

    # Get post content.
    content = [] of Posts
    DB.open "sqlite3://./data.db" do |db|
        content = Posts.from_rs(db.query("select * from post where id = #{post_id}"))
    end

    # Get post comments.
    comment = [] of Comments
    DB.open "sqlite3://./data.db" do |db|
        comment = Comments.from_rs(db.query("select * from comment where post_id = #{post_id}  order by id desc"))
    end

    # Styles for text: green text (>) and reply (>>).
    match_green = /((^>)[^>]\w+)/
    match_link = /(>>(\d+))/
    green_text = "<p class='green-text'>\\1</p>"
    link_text = "<a href='#\\2' class='link-text' onclick='highlight(\\2)'>\\1</a> "

    render "src/views/paja.ecr"
end

# Paja chat.
get "/chat" do |_|
    render "src/views/chat.ecr"
end

messages = [] of String
sockets = [] of HTTP::WebSocket

ws "/chat" do |socket|
  sockets.push socket

  # Handle incoming message and dispatch it to all connected clients.
  socket.on_message do |message|
      messages.push message
      sockets.each do |a_socket|
          a_socket.send message
      end
  end

  # Disconnect client from socket.
  socket.on_close do |_|
    sockets.delete(socket)
    puts "Closing Socket: #{socket}"
  end
end

# Paja game.
get "/game" do
    id = rand_id
    render "src/views/game.ecr"
end

# Paja game: room.
get "/game/:id" do |env|
    id = env.params.url["id"]
    render "src/views/game-room.ecr"
end

ws "/game/:id" do |socket|
  socket.on_message do |message|
      puts "New player"
      messages.push message
      sockets.each do |a_socket|
          puts "New player2"
          a_socket.send message
      end
  end
end

error 404 do |env|
    code = env.response.status_code
    render "src/views/error.ecr"
end

error 403 do |env|
    code = env.response.status_code
    render "src/views/error.ecr"
end

Kemal.config.port = 1488
Kemal.run

